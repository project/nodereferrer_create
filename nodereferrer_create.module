<?php

/**
 * @file nodereferrer_create.module
 * allow site admins to easily add inline forms to nodes for creating new referencing nodes
 *
 * todo:
 *  - correctly alter autocomplete widgets
 *  - review and optimize permission checking routines
 */

/**
 * Implementation of hook_menu().
 */
 
function nodereferrer_create_menu($may_cache) {
  $items = array();

  if (!$may_cache) {
    if (arg(0) == 'node' AND is_numeric(arg(1))) {
      if ($node = node_load(arg(1))) {
        // Todo: this is all very inefficient
        $type = content_types($node->type);
        foreach($type['fields'] AS $name => $field) {
          if ($field['type'] == 'nodereferrer' AND $field['widget']['type'] == 'nodereferrer_create_list' AND is_array($field['referrer_types'])) {
            // May the user create at least one of the node-types covered by this field?
            foreach(array_filter($field['referrer_types']) AS $reftype) {
              if (node_access('create', $reftype)) {
                $items[] = array(
                  'path' => 'node/'.arg(1).'/add/'.substr($name, strlen('field_')),
                  'title' => $field['widget']['title'],
                  'type' => MENU_LOCAL_TASK,
                  'callback' => 'nodereferrer_create_add',
                  'callback arguments' => array($field, $node),
                  'weight' => 10,
                );
                break;
              }
            }
          }
        }
      }
    }
  }
  return $items;
}

/**
 * Adapted from node_add
 *
 * @param $base_field
 *   nodereferrer field array
 * @param $base_node
 *   base node object
 * @param $type 
 *   type of node to create
 * @param $alter_field
 *   name of field to alter
 * @return 
 *   themed form
 */
function nodereferrer_create_add($base_field, $base_node, $type=NULL, $alter_field=NULL) {
  global $user;
  $output = '';
  drupal_set_title(check_plain($base_node->title));
  foreach(array_filter($base_field['referrer_types']) AS $reftype) {
    if (node_access('create', $reftype)) {
      $types[] = $reftype;
    }
  }
  $fields = array_filter($base_field['referrer_fields']);

  // skip menu if only one type/field combo is available
  if (count($types) == 1 AND count($fields) == 1) {
    $type = $types[0];
    $alter_field = current($base_field['referrer_fields']);
  }
  // If a node type has been specified, validate its existence.
  if (isset($type) AND in_array($type, $types) AND isset($alter_field)) {
    //signal that form should be altered
    nodereferrer_create_alter_signal(array('type' => $type, 'field' => $alter_field, 'nid' => $base_node->nid));
    
    //Initialize new node
    $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => $type);
    $output .= drupal_get_form($type .'_node_form', $node);
  } else {
    $type_info = content_types();

    //could do: differentiate between fields with same type, but I don't have a use-case
    foreach ($types AS $typename) {
      foreach ($fields AS $fieldname) {
        if (isset($type_info[$typename]['fields'][$fieldname]));
        $menu[$fieldname][] = $typename;
        $items[] = l($type_info[$typename]['name'], 'node/'.$base_node->nid.'/add/'.substr($base_field['field_name'], strlen('field_')).'/'.$typename.'/'.$fieldname);
      }
    }
    $output .= theme('item_list', $items);
  }
  return $output;
}

/**
 * allow nodereferrer_create_add to signal that the node form should be altered
 * @param $set 
 *   an array like ('nid'=>1,'type'=>'page', 'field'=>'name')
 * @return
 *   if no param is passed, return the stored array
 */
function nodereferrer_create_alter_signal($set=NULL) {
  static $return;
  if ($set) {
    $return = $set;
  }
  else return $return;
}
    
function nodereferrer_create_form_alter($form_id, &$form) {
  if ($signal = nodereferrer_create_alter_signal() AND $form_id == $signal['type'].'_node_form') {
    // This only works if the widget type is select list, not for autocomplete
    $form[$signal['field']]['nids'] = array( '#type' => 'value', '#value' => $signal['nid']);
  }
}

/**
 * Implementation of hook_widget_info().
 */
function nodereferrer_create_widget_info() {
  return array(
    'nodereferrer_create_list' => array(
      'label' => t('Read-Only List With Context Adding'),
      'field types' => array('nodereferrer'),
    ),
  );
}

/**
 * Implementation of hook_widget().
 */
function nodereferrer_create_widget($op, &$node, $field, &$node_field) {
  return nodereferrer_widget($op, &$node, $field, &$node_field);
}

function nodereferrer_create_widget_settings($op, $widget) {  
  switch ($op) {  
    case 'form':  
      $form = array();  
      $form['title'] = array(  
        '#type' => 'textfield',  
        '#title' => t('Title for create tab'),  
        '#default_value' => $widget['title']? $widget['title']:'Add '.$widget['label'],  
        '#required' => TRUE,  
      );  
      return $form;  
 
    case 'validate':
      break;
    case 'save':  
      return array('title');  
  }  
}  
 
